# Image gallery Search #
### What is this repository for? ###
* Service to search image data based on attributes (author, camera, tags) using AgileEngine api service.

### How do I get set up? ###
* The project is settled up with docker so you need to install:
    * [Docker](https://www.docker.com)
    * [Docker compose](https://docs.docker.com/compose/install/)
* Run builder `docker-compose build`
* Run project `docker-compose up`

### Special commands ###

* To fecth all pictures we must run the django command (fetch_pictures_command), to run it just execute `docker-compose run django python backend/manage.py fetch_pictures_command`
* To fetch all details we must run the django command (fetch_picture_details_command), to run it just execute `docker-compose run django python backend/manage.py fetch_picture_details_command`
* These commands should be croned to be executed automatlically.

### To search ###

* We created a new endpoint `/search/?search=${searchTerm}`