from django.db import models


class AgileEngineToken(models.Model):
    token = models.TextField()
    generated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.token
