import requests
from services.models import AgileEngineToken


class AgileEngineService:
    HOST = 'http://interview.agileengine.com'
    API_KEY = '23567b218376f79d9415'

    def __init__(self):
        super().__init__()

    def _get_client(self):
        """
        It returns a request object with
        """
        api_client = requests.Session()
        api_client.headers = {
            'apiKey': self.API_KEY,
        }
        return api_client

    def get_token(self, new=False):
        """
        If the token is not expired it returns it.
        But, if the token is expired, we will get a new one.
        """
        token_not_expired = AgileEngineToken.objects.all()
        if new or not token_not_expired.exists():
            # Set connection with api service
            # Hit to the endpoint
            api_client = self._get_client()
            try:
                response = api_client.post(
                    f'{self.HOST}/auth',
                    json={
                        'apiKey': self.API_KEY,
                    }
                )
                token = response.json().get('token')
            except Exception as e:
                # NOTE: Here I Should retry the call if it fails
                raise Exception(e)

            # Save the new token obteined
            AgileEngineToken.objects.create(
                token=token,
            )
        else:
            token_instance = token_not_expired.last()
            token = token_instance.token

        return token

    def fetch_images(self, page=1):
        token = self.get_token()
        api_client = self._get_client()
        try:
            response = api_client.get(
                f'{self.HOST}/images',
                params={
                    'page': page,
                },
                headers={
                    'Authorization': f'Bearer {token}',
                },
            )
        except Exception as e:
            # NOTE: Here I Should retry the call if it fails
            raise Exception(e)

        return response.json()

    def fetch_image_detail(self, image_id):
        token = self.get_token()
        api_client = self._get_client()
        try:
            response = api_client.get(
                f'{self.HOST}/images/{image_id}',
                headers={
                    'Authorization': f'Bearer {token}',
                },
            )
        except Exception as e:
            # NOTE: Here I Should retry the call if it fails
            raise Exception(e)

        return response.json()
