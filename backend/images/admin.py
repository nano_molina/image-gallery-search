from django.contrib import admin
from images.models import Picture, PictureDetail

admin.site.register(Picture)
admin.site.register(PictureDetail)
