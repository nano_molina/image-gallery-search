# Generated by Django 3.1.2 on 2020-11-02 05:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('images', '0003_auto_20201102_0527'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='picturedetail',
            name='picture',
        ),
        migrations.AddField(
            model_name='picture',
            name='picture',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='picture', to='images.picturedetail'),
        ),
    ]
