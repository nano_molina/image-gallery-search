from images.models import PictureDetail
from rest_framework import serializers


class PictureDetailSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = PictureDetail
        fields = [
            'author',
            'camera',
            'tags',
            'cropped_picture',
            'full_picture',
        ]
