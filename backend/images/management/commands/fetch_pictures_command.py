from django.core.management.base import BaseCommand
from images.models import Picture
from services.service import AgileEngineService


class Command(BaseCommand):
    help = 'It fetch all images'

    def update_pictures(self, pictures):
        """
        Given a list with json pictures data
        it updates the database.
        """
        for picture in pictures:
            Picture.objects.get_or_create(
                service_id=picture.get('id'),
                cropped_picture=picture.get('cropped_picture'),
            )

    def handle(self, *args, **options):
        """
        It iterates all pages in external service
        and update local database.
        """
        self.stdout.write(self.style.SUCCESS('Starting fetching'))

        service = AgileEngineService()
        has_more = True
        page = 1

        while has_more:
            self.stdout.write(self.style.SUCCESS(f'Page {page}'))
            json_response = service.fetch_images(page=page)
            self.update_pictures(pictures=json_response.get('pictures'))
            has_more = json_response.get('hasMore')
            page += 1

        self.stdout.write(self.style.SUCCESS('fetching finished'))
