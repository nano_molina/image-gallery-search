from django.core.management.base import BaseCommand
from images.models import Picture, PictureDetail
from services.service import AgileEngineService


class Command(BaseCommand):
    help = 'It fetch all picture details'

    def handle(self, *args, **options):
        """
        It iterates all pictures and update them with the external service.
        """
        self.stdout.write(self.style.SUCCESS('Starting fetching'))

        service = AgileEngineService()

        # NOTE: I should paginate this iteration.
        for picture in Picture.objects.select_related('detail').all():
            json_response = service.fetch_image_detail(
                image_id=picture.service_id
            )
            self.update_or_create_details(
                picture=picture,
                details=json_response
            )

        self.stdout.write(self.style.SUCCESS('fetching finished'))

    def update_or_create_details(self, picture, details):
        # It updates information when is necesary
        # I should use a serializer but for the future :)
        author = details.get('author')
        camera = details.get('camera')
        tags = details.get('tags')
        cropped_picture = details.get('cropped_picture')
        full_picture = details.get('full_picture')

        if picture.detail:
            data_to_update = {
                'picture': picture,
            }
            # Check data to update
            if not picture.detail.author == author:
                data_to_update.update({'author': author})
            if not picture.detail.camera == camera:
                data_to_update.update({'camera': camera})
            if not picture.detail.tags == tags:
                data_to_update.update({'tags': tags})
            if not picture.detail.cropped_picture == cropped_picture:
                data_to_update.update({'cropped_picture': cropped_picture})
            if not picture.detail.full_picture == full_picture:
                data_to_update.update({'full_picture': full_picture})
            # Update instance in database
            PictureDetail.objects.filter(picture_id=picture.id).update(
                **data_to_update
            )
        else:
            new_detail = PictureDetail.objects.create(
                picture=picture,
                author=author,
                camera=camera,
                tags=tags,
                cropped_picture=cropped_picture,
                full_picture=full_picture,
            )
            picture.detail = new_detail
            picture.save(update_fields=['detail'])

