from django.urls import path
from images import api_views


urlpatterns = [
    path('', api_views.PictureDetailListApiView.as_view()),
]
