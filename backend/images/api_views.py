from rest_framework.generics import ListAPIView
from images.serializers import PictureDetailSerializer
from images.models import PictureDetail
from rest_framework import filters


class PictureDetailListApiView(ListAPIView):
    queryset = PictureDetail.objects.all()
    serializer_class = PictureDetailSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['author', 'camera', 'tags']
