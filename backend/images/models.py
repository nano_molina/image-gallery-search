from django.db import models


class PictureDetail(models.Model):
    author = models.CharField(
        max_length=200,
        db_index=True,
        null=True, blank=True,
    )
    camera = models.CharField(
        max_length=200,
        db_index=True,
        null=True, blank=True,
    )
    tags = models.CharField(
        max_length=200,
        db_index=True,
        null=True, blank=True,
    )
    cropped_picture = models.URLField(
        max_length=200,
        null=True, blank=True,
    )
    full_picture = models.URLField(
        max_length=200,
        null=True, blank=True,
    )

    def __str__(self):
        return f'{self.id}'


class Picture(models.Model):
    service_id = models.CharField(
        unique=True,
        max_length=200,
        db_index=True,
    )
    cropped_picture = models.URLField(
        max_length=200
    )
    detail = models.OneToOneField(
        PictureDetail,
        on_delete=models.CASCADE,
        related_name='picture',
        null=True, blank=True,
    )

    def __str__(self):
        return self.service_id
